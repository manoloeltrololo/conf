import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from typing import List  # noqa: F401

mod = "mod4"                                     # Sets mod key to SUPER/WINDOWS
myTerm = "alacritty"                             # My terminal 

keys = [

        # ------------ Qtile buttons ------------

            Key([mod, "shift"], "r", lazy.restart(), desc='Restart Qtile'),
            Key([mod, "shift"], "q", lazy.shutdown(), desc='Shutdown Qtile'),

        # ------------ Window Configs ------------

            Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
            Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
            Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
            Key([mod], "k", lazy.layout.up(), desc="Move focus up"),

            Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
            Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
            Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
            Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

            Key([mod, "shift"], "f", lazy.window.toggle_floating(), desc='toggle floating'),
            Key([mod], "f", lazy.window.toggle_fullscreen(), desc='toggle fullscreen'),
            
            Key([mod], "w", lazy.window.kill(), desc='Kill active window'),

            Key([mod], "Tab", lazy.next_layout(), desc='Toggle through layouts'),
            Key([mod, "shift"], "Tab", lazy.layout.rotate(), lazy.layout.flip(), desc='Switch which side main pane occupies (XmonadTall)'),

            Key([mod], "space", lazy.layout.next(), desc='Switch window focus to other pane(s) of stack'),
            Key([mod, "shift"], "space", lazy.layout.toggle_split(), desc='Toggle between split and unsplit sides of stack'),

        # ------------ Monitor Focus ------------

            Key([mod], "period", lazy.next_screen(), desc='Move focus to next monitor'),
            Key([mod], "comma", lazy.prev_screen(), desc='Move focus to prev monitor'),

        # ------------ App Configs ------------

            Key([mod], "Return", lazy.spawn(myTerm+" -e fish"), desc='Launches My Terminal'),
            Key([mod, "shift"], "Return", lazy.spawn("rofi -show drun -display-drun \"Run: \" -drun-display-format \"{name}\""), desc='Run Launcher'),

        # ------------ Hardware Configs ------------

         Key([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%")),
         Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%")),
         Key([], "XF86AudioMute", lazy.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle")),

         Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl set +10%")),
         Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl set 10%-")),

    ]

groups = [Group(i) for i in [
    "", "", "", "", "", "", "", "", "",
]]

for i, group in enumerate(groups):
    actual_key = str(i + 1)
    keys.extend([
        # Switch to workspace N
        Key([mod], actual_key, lazy.group[group.name].toscreen()),
        # Send window to workspace N
        Key([mod, "shift"], actual_key, lazy.window.togroup(group.name))
    ])


colors =  [

        ["#282c34", "#282c34"], # panel background
        ["#528bff", "#528bff"], # background active window
        ["#bfbfbf", "#bfbfbf"], # font color
        ["#ff5555", "#ff5555"], # widget color
        ["#fd924d", "#fd924d"], # widget color
        ["#7fda55", "#7fda55"], # widget color
        ["#c6e284", "#c6e284"], # widget color
        ["#bd93f9", "#bd93f9"]] # widget color

layouts = [
    layout.MonadTall(margin=8, border_width=4, border_focus="#4b5263", border_normal="#282c34"),
    layout.MonadWide(margin=8, border_width=4, border_focus="#4b5263", border_normal="#282c34"),
    layout.Bsp      (margin=8, border_width=4, border_focus="#4b5263", border_normal="#282c34", fair=False),
    layout.Max(),
]


widget_defaults = dict(
    font='Mononoki Nerd Font bold',
    fontsize=18,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        wallpaper='/home/santito/wallpapers/astronaut.jpg',
        wallpaper_mode='fill',
        top=bar.Bar(
            [
                widget.CurrentLayoutIcon(
                    custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
                    scale=0.45,
                    padding=0,
                ),

                widget.GroupBox(
                    fontsize=32,
                    margin_y=5,
                    margin_x=2,
                    padding_y=5,
                    padding_x=4,
                    borderwidth=5,
                    active=colors[1],
                    inactive=colors[2],
                    rounded=True,
                    highlight_color=colors[0],
                    highlight_method="line",
                    this_current_screen_border=colors[1],
                ),
                widget.WindowName(
                       foreground = colors[1],
                       background = colors[0],
                       padding = 0
                ),

                widget.Sep(
                    padding=4,
                    linewidth=0,
                    background=colors[0],
                ),
                
                widget.Clock(
                    foreground=colors[2],
                    background=colors[0],
                    fontsize=20,
                    format='%H:%M'
                ),

                widget.Spacer(),

                widget.Sep(
                    padding=4,
                    linewidth=0,
                    background=colors[0],
                ),
                widget.Systray(
                    background=colors[0],
                    icons_size=20,
                    padding=4,
                ),
                widget.Sep(
                    padding=8,
                    linewidth=0,
                    background=colors[0],
                ),
                
                widget.Net(
                       interface = "wlp2s0",
                       format = '{down} ↓↑ {up}',
                       foreground = colors[7],
                       background = colors[0],
                       ),

                widget.Sep(
                    padding=8,
                    linewidth=0,
                    background=colors[0],
                ),

                widget.Memory(
                    background=colors[0],
                    foreground=colors[3],
                    fontsize=18,
                    mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                    format=' {MemUsed: .0f} MB'
                ),
                widget.Sep(
                    padding=8,
                    linewidth=0,
                    background=colors[0],
                ),
                widget.TextBox(
                    text='墳 =',
                    fontsize='18',
                    padding=0,
                    background=colors[0],
                    mouse_callbacks={'Button3': lambda: qtile.cmd_spawn("pavucontrol")},
                    foreground=colors[4],
                ),
                widget.PulseVolume(
                    background=colors[0],
                    foreground=colors[4],
                    fontsize=18,
                    limit_max_volume="True",
                    mouse_callbacks={'Button3': lambda: qtile.cmd_spawn("pavucontrol")},
                ),
                widget.Sep(
                    padding=6,
                    linewidth=0,
                    background=colors[0],
                ),

                widget.Clock(
                    foreground=colors[5],
                    background=colors[0],
                    fontsize=18,
                    format=' %A %d %b',
                ),
                widget.Sep(
                    padding=6,
                    linewidth=0,
                    background=colors[0],
                ),

                widget.Battery(
                    foreground=colors[6],
                    background=colors[0],
                    fontsize=18,
                    update_interval=1,
                    format=' {percent:2.0%}',
                ),
                widget.Sep(
                    padding=6,
                    linewidth=0,
                    background=colors[0],
                ),
            ],
        40,
            opacity=0.9,
            background=colors[0],
            margin=[6,8,0,8]
            ),
        
       ),
    ]

def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)


# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    # default_float_rules include: utility, notification, toolbar, splash, dialog,
    # file_progress, confirm, download and error.
    *layout.Floating.default_float_rules,
    Match(title='Confirmation'),      # tastyworks exit box
    Match(title='Qalculate!'),        # qalculate-gtk
    Match(wm_class='kdenlive'),       # kdenlive
    Match(wm_class='pinentry-gtk-2'), # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# ------------ AutoStart ------------

@hook.subscribe.startup_once
def autostart():
    processes = [
        ['connman-gtk'],
        ['picom']
    ]

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
